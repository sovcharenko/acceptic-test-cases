<?php
/**
 * Index
 */
define('DIR_ROOT', __DIR__);

require_once DIR_ROOT . '/vendor/autoload.php';

use accepticTest\helpers\SimpleValidator;

$sv = new SimpleValidator();

$valuesToValidate = [
    'user_name' => 'Sere25ga',
    'user_email' => 'Sere25@.com',
    'user_age' => 15,
];

$sv->load($valuesToValidate);

$sv->isEmail('user_email');
$sv->isAlphabetical('user_name');
$sv->strLengthBetween('user_name', 2, 30);
$sv->greaterThan('user_age', 21);

if ($sv->validate()) {
    echo 'Success';
} else {
    echo 'Error';
    echo '<br>';
    echo '<pre>';
    var_dump( $sv->getErrors() );
    echo '</pre>';

}