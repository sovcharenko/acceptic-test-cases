<?php

namespace accepticTest\helpers;

class SimpleValidator implements ValidatorInterface
{

    public function load($attributes = [])
    {
        // TODO: Implement load() method.
    }

    public function getError($attributeName = '')
    {
        // TODO: Implement getError() method.
    }

    public function required($attributeName = '')
    {
        // TODO: Implement required() method.
    }

    public function greaterThan($attributeName = '', $compareValue = 0)
    {
        // TODO: Implement greaterThan() method.
    }

    public function lowerThan($attributeName = '', $compareValue = 0)
    {
        // TODO: Implement lowerThan() method.
    }

    public function between($attributeName = '', $compareValueMin = 0, $compareMax = 0)
    {
        // TODO: Implement between() method.
    }

    public function strLongerThan($attributeName = '', $compareValue = 0)
    {
        // TODO: Implement strLongerThan() method.
    }

    public function strLessThan($attributeName = '', $compareValue = 0)
    {
        // TODO: Implement strLessThan() method.
    }

    public function strLengthBetween($attributeName = '', $compareValueMin = 0, $compareMax = 0)
    {
        // TODO: Implement strLengthBetween() method.
    }

    public function strLength($attributeName = '', $compareValue = 0)
    {
        // TODO: Implement strLength() method.
    }

    public function isNumeric($attributeName = '')
    {
        // TODO: Implement isNumeric() method.
    }

    public function isAlphabetical($attributeName = '')
    {
        // TODO: Implement isAlphabetical() method.
    }

    public function isEmail($attributeName = '')
    {
        // TODO: Implement isEmail() method.
    }

    public function isUrl($attributeName = '')
    {
        // TODO: Implement isUrl() method.
    }

    public function isTrue($attributeName = '')
    {
        // TODO: Implement isTrue() method.
    }

    public function isIn($attributeName = '', $compareArray = [])
    {
        // TODO: Implement isIn() method.
    }

    public function isNotIn($attributeName = '', $compareArray = [])
    {
        // TODO: Implement isNotIn() method.
    }

    public function equals($attributeName1 = '', $attributeName2 = '')
    {
        // TODO: Implement equals() method.
    }

    public function validate()
    {
        // TODO: Implement validate() method.
        return true;
    }

    public function getErrors()
    {
        // TODO: Implement getErrors() method.
    }

    public function strLengthEquals($attributeName = '', $compareValue = 0)
    {
        // TODO: Implement strLengthEquals() method.
    }
}