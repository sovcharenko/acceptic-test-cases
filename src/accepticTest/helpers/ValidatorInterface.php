<?php

namespace accepticTest\helpers;


interface ValidatorInterface
{
    /**
     * Вначале загружаем аттрибуты в виде ассоциативного массива:
     * - Ключ - имя аттрибута
     * - Значение - значение аттрибута
     * Пример:
     * $attributes = [
     *      'user_email' => 'user@ma.il',
     *      'user_age' => 25,
     *      'user_name' => 'Tester'
     * ]
     *
     * @param array $attributes
     * @return void
     */
    public function load( $attributes = [] );

    /**
     * Обязательное значение. Строка пустая - невалид, значение null - невалид, но если значение === 0 - валид
     *
     * @param string $attributeName
     * @return void
     */
    public function required( $attributeName = '' );

    /**
     * Сравнение чисел
     *
     * @param string $attributeName
     * @param int $compareValue
     * @return void
     */
    public function greaterThan( $attributeName = '', $compareValue = 0 );
    public function lowerThan( $attributeName = '', $compareValue = 0 );
    public function between( $attributeName = '', $compareValueMin = 0, $compareMax = 0 );

    /**
     * Сравнение длинны строки
     *
     * @param string $attributeName
     * @param int $compareValue
     * @return void
     */
    public function strLongerThan( $attributeName = '', $compareValue = 0 );
    public function strLessThan( $attributeName = '', $compareValue = 0 );
    public function strLengthBetween( $attributeName = '', $compareValueMin = 0, $compareMax = 0 );
    public function strLengthEquals( $attributeName = '', $compareValue = 0);

    /**
     * Более сложное сравнение
     *
     * @param string $attributeName
     * @return void
     */
    public function isNumeric( $attributeName = '' ); // только числа
    public function isAlphabetical( $attributeName = '' ); // только буквы
    public function isEmail( $attributeName = '' ); // валидный имейл
    public function isUrl( $attributeName = '' ); // валидная ссылка
    public function isTrue($attributeName = ''); // значение не FALSE (true, 1, 'true', 'yes' = TRUE; false, 0, 'false', 'no' = FALSE)
    public function isIn($attributeName = '', $compareArray = []);
    public function isNotIn($attributeName = '', $compareArray = []);
    public function equals($attributeName1 = '', $attributeName2 = '');

    /**
     * Проходиться по атрибутам и валидирует их соответственно установленным правилам.
     * Если хоть одно правило не проходит - возвращает FALSE, если всё валидно - TRUE
     *
     * @return bool
     */
    public function validate();

    /**
     * Возвращает удобные чтабельные сообщения для тех полей, у которых валидация не прошла.
     * Если валидация везде прошла - возвращает FALSE.
     * Пример возвращаемого значения когда валидация не прошла:
     * [
     *      'user_email' => 'This is not valid Email Address',          // isEmail( 'user_email' );
     *      'user_name' => 'This field should contain only letters',    // isAlphabetical( 'user_name' );
     *      'user_age' => 'This should be greater than 21',             // greaterThan( 'user_age', 21 );
     * ]
     *
     * @return array|bool
     */
    public function getErrors();

    /**
     * Возвращает то же самое что и предыдущая функция, но для одного указанного аттрибута (строку)
     *
     * @param string $attributeName
     * @return string|bool
     */
    public function getError( $attributeName = '' );

}